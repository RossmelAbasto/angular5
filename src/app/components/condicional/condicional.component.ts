import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-condicional',
  templateUrl: './condicional.component.html',
  styleUrls: ['./condicional.component.scss']
})
export class CondicionalComponent implements OnInit {
  
  persona: any = {
    nombre: 'Rossmel Abasto',
    direccion: 'Calle falsa 123'
  }
  mostrar: boolean = true;
  
  constructor() {
  }

  ngOnInit(): void {
  }

}
